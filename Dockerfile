##
# Multistage build for the Year Progress Bot
#
# Dockerfile references https://docs.docker.com/engine/reference/builder/
# Multistage builds https://docs.docker.com/develop/develop-images/multistage-build/
##


# Stage 1/2, Build

# Use latest golang base image
FROM golang:latest AS builder

# Enable GO111MODULES, requires at least Go 1.11
ENV GO111MODULE=on

# Set the working directory
WORKDIR /app

# Copy everything from the current directory to the present working directory inside the container
COPY . .

RUN go mod download

ARG OS=linux
ARG ARCH=amd64
RUN CGO_ENABLED=0 GOOS=${OS} GOARCH=${ARCH} go build -o main


# Stage 2/2, Final

# Use latest alpine image, "FROM scratch" should also work, but I haven't tested that (yet)
FROM alpine:latest

# Add maintainer info
LABEL maintainer="Florian \"KopfKrieg\" <dev@absolem.cc>"

# Copy app from builder
WORKDIR /app
COPY --from=builder /app/main .

ENTRYPOINT ["/app/main"]

