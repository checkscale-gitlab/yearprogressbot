package main

// Imports
import (
	"gitlab.com/KopfKrieg/gotodon"
	"gitlab.com/KopfKrieg/gotodonr"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

// Constants
const (
	configPath     = "/gotodon.json"
	clientName     = "Year Progress Bot"
	website        = "https://gitlab.com/KopfKrieg/yearprogressbot"
	dark           = "▓"
	light          = "░"
	steps      int = 20 // Must be divisible by 100
)


// Function to register client, get access token, and store the configuration
func register(server *string, username *string, password *string) error {
	// Create new client
	client := gotodon.CreateClient(*server, gotodonr.Scopes(gotodonr.ScopeRead, gotodonr.ScopeWrite, gotodonr.ScopeFollow, gotodonr.ScopePush), "", "")

	// Register client
	err := client.Register(clientName, website)
	if err != nil {
		return err
	}

	// Password Grant Flow
	err = client.AuthorizeWithLoginOnce(*username, *password)
	if err != nil {
		return err
	}

	// Save configuration
	err = client.Store(configPath)
	if err != nil {
		return err
	}

	// Return
	return nil
}


// Loads configuration and posts status
func postStatus(msg *string) error {
	// Create client from configuration file
	err, client := gotodon.LoadClient(configPath)
	if err != nil {
		return err
	}

	// Post status
	_, err = client.PostTextStatus(*msg, "", false, "", gotodonr.VisibilityPublic, "en", "")
	if err != nil {
		return err
	}

	// Store new configuration
	err = client.Store(configPath)
	if err != nil {
		return err
	}

	// Return
	return nil
}


// Main function
func main() {
	// Environment variables
	dryRun := false
	server := os.Getenv("SERVER")
	username := os.Getenv("USERNAME")
	password := os.Getenv("PASSWORD")  // TODO: Implement safe handling of sensitive environment variables

	// Dry run?
	_, ok := os.LookupEnv("DRYRUN")
	if ok {
		dryRun = true
		log.Println("Dry run selected, skipping wait times and mastodon features")
	}

	// Register client
	err := register(&server, &username, &password)
	if err != nil {
		log.Fatal(err)
	}

	// Calculate everything™
	// Get current time, start and end of year
	timestampNow := time.Now()
	timestampStart := time.Date(timestampNow.Year(), 1, 1, 0, 0, 0, 0, time.UTC)
	timestampEnd := time.Date(timestampNow.Year(), 12, 31, 23, 59, 59, 0, time.UTC)

	// Get elapsed time from timestampStart to timestampEnd and elapsedTime since timestampStart
	durationYear := timestampEnd.Sub(timestampStart)
	elapsedTime := timestampNow.Sub(timestampStart)

	// Convert to seconds
	durationYearSeconds := float64(durationYear / time.Second)
	elapsedTimeSeconds := float64(elapsedTime / time.Second)

	// Calculate progress and convert to int
	progress := int(elapsedTimeSeconds / durationYearSeconds * 100)

	// This is our goal, this is when our bot should publish the next post
	progressNext := progress + 1

	// Loop variables
	var progressNextSeconds float64
	var progressNextTimestamp time.Time
	var timeToSleep time.Duration
	var msg string

	// Forever running loop
	for true {
		// Is the year already over?!
		if progressNext > 100 {
			// TODO The year ends, I should implement a "Happy New Year" message here :)
			panic("The end is neigh!")
		}

		// And now reverse the whole thing
		progressNextSeconds = float64(progressNext) / 100.0 * durationYearSeconds
		progressNextTimestamp = timestampStart.Add(time.Duration(progressNextSeconds) * time.Second)

		// Sleep until we're there…
		timeToSleep = time.Until(progressNextTimestamp)
		log.Printf("Waiting until %s (%s)\n", progressNextTimestamp, timeToSleep)

		if !dryRun {
			time.Sleep(timeToSleep)
		}

		// Create message, e.g.: "▓▓▓▓▓▓▓▓▓░░░░░░░░░░░ 45%"
		msg = strings.Repeat(dark, progressNext/(100/steps))
		msg += strings.Repeat(light, steps-progressNext/(100/steps))
		msg += " " + strconv.Itoa(progressNext) + "%"

		if progressNext == 100 {
			msg += "\n\n🎉 Happy New Year! 🎉\n\n🍾🥳"
		}

		log.Println(msg)

		// Publish post
		if !dryRun {
			err := postStatus(&msg)
			if err != nil {
				// Wait ten minutes, then try a second time
				log.Println("First try to post status message failed, trying again in 10 minutes…")
				time.Sleep(10 * time.Minute)
				err = postStatus(&msg)
				if err != nil {
					// Wait one hour, then try a third time
					log.Println("Second try to post status message failed, trying again in 1 hour")
					time.Sleep(1 * time.Hour)
					err = postStatus(&msg)
					if err != nil {
						log.Fatalf("Third try also failed, this is a fatal error now. Original error message is below\n%s", err)
					}
				}
			}
		}

		// And set the goal for our next toot
		progressNext += 1
	}
}
